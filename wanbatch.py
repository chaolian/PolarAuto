#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  8 11:21:30 2021

@author: clian
"""
import os
from pp import postprocess
from file_heads import atomic_masses, atomic_numbers
from file_heads import system_line, position_line, control_line, kpoint_line
from file_heads import setting, kpoint_band, ph_file, ph_g_file, get_nscf_mesh
from file_heads import epw_wan_in, epw_dvscf_in, path_qpt

nkpoints = (4, 4, 4)
nqpoints = (1, 1, 1)
np = 16; nk = 16

dryrun = True
run_opti = True
run_scf  = True
run_nscf = True
run_band = True
run_ph   = True
run_g_ph = True

os.environ["EPW"] = '/home/clian/Documents/Documents/Projects/EPW/q-e/'
os.environ["ESPRESSO_PSEUDO"] = "/home/clian/Documents/Documents/Projects/PseudoPotential/dojo-sr-pbe"
os.environ["MPIRUN"] = "mpirun"

structSet = [[('Ga','As'), 6.847, 0.25], 
             #[('Si','Si'), 6.847, 0.25],
             #[('Na','Cl'), 6.847, 0.5],
             #[('Ca','O'), 6.847, 0.5],
             ]

def run(command, dryrun = True):
    if not dryrun: os.system(command)
        for compound, alat, pos  in structSet: #['Ga','As'], ['Si', 'Si']: #['B','N'], 
            if compound[0] == compound[1]:
                dirName = compound[0]
            else:
                dirName = ''.join(compound)
                
            print("Calculating the structure : ", dirName)
            
            ###################################### Build Input ###########################################
            curdir = os.path.abspath('.')
            
            elements = []
            for element in compound:
                index = atomic_numbers[element]
                elements.append([element, index, atomic_masses[index]])
            
            if not os.path.exists(dirName):
                os.mkdir(dirName)
            os.chdir(dirName)
        
            numElement = len(elements)
            
            species = 'ATOMIC_SPECIES \n'
            for i, element in enumerate(elements):
                psedo  = element[0] +'.upf'
                species += "%s \t %.4f \t %s\n" % (element[0], element[2], psedo)
            
            # Get 
            nelectron = 0
            for element in compound:
                psedo  = element +'.upf'
                nElement = [float(line.split()[-1][:-1]) for line in 
                            open(os.environ["ESPRESSO_PSEUDO"] + '/' + psedo) 
                            if 'z_valence=' in line][-1]
                nelectron += nElement
            
            nbnd_occ = int(nelectron)//2
            print(nelectron, nbnd_occ)
            nbnd_unocc = 10
            nbnd = nbnd_occ + nbnd_unocc #int(nelectron)
            ###################################### EPW HOLE ################################################
            if not os.path.exists('hpolaron'): os.mkdir('hpolaron')
            os.chdir('hpolaron')
            os.system('ln -s ../ph/save . 2>/dev/null')
            os.system('ln -s ../nscf/pwscf.save . 2>/dev/null')
            open('path.qpt', 'w').write(path_qpt)
        
            nbndsub = 4
            if nbnd_occ > nbndsub:
                addline = "bands_skipped = 'exclude_bands = 1:%i, %i:%i'"%(nbnd_occ - nbndsub, 
                                                                           nbnd_occ+1, nbnd)
            wan_file = epw_wan_in%(nbndsub, addline, '%s:sp3'%compound[0], *nkpoints, *nqpoints)
            
            open('epw.wan.in', 'w').write(wan_file)
            command = '$MPIRUN -np %i $EPW/bin/epw.x -nk %i -i epw.wan.in | tee epw.wan.out'%(np, nk)
            wan_file = epw_dvscf_in%(nbndsub, addline, *nkpoints, *nqpoints)
            open('epw.dvscf.in', 'w').write(wan_file)
            os.chdir('..')
            ################################### EPW ELECTRON ###############################################
            
            os.system('mkdir epolaron 2>/dev/null')
            os.chdir('epolaron/')
            os.system('ln -s ../save . 2>/dev/null')
            os.system('ln -s ../pwscf.save . 2>/dev/null')
            open('path.qpt', 'w').write(path_qpt)
            
            nbndsub = 4
            addline = "bands_skipped = 'exclude_bands = 1:%i, %i:%i'"%(nbnd_occ, 
                                                                       nbnd_occ + nbndsub + 1, nbnd)
            wan_file = epw_wan_in%(nbndsub, addline, '%s:sp3'%compound[0], *nkpoints, *nqpoints)
            open('epw.wan.in', 'w').write(wan_file)
            
            
            wan_file = epw_dvscf_in%(nbndsub, addline, *nkpoints, *nqpoints)
            open('epw.dvscf.in', 'w').write(wan_file)
            command = '$MPIRUN -np %i $EPW/bin/epw.x -nk %i -i epw.wan.in | tee epw.wan.out'%(np, nk)
            #run(command, dryrun)
            os.chdir('..')
    
    