#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  9 16:38:18 2021

@author: clian
"""
import os 
import AutoFlow.fccbatch as fc

fc.nkpoints = (8, 8, 8)
fc.nqpoints = (1, 1, 1)
fc.np = 16; fc.nk = 16
fc.ecut = 120.0

fc.dryrun   = False
fc.run_opti = True
fc.run_scf  = True
fc.run_nscf = True
fc.run_band = True
fc.run_ph   = True
fc.run_g_ph = True

os.environ["EPW"] = '/home/clian/Documents/Documents/Projects/EPW/q-e/'
os.environ["ESPRESSO_PSEUDO"] = "/home/clian/Documents/Documents/Projects/PseudoPotential/dojo-sr-pbe"
os.environ["MPIRUN"] = "mpirun"

fc.structSet = [[('Ga','As'), 7.757, 0.25], 
             #[('Si','Si'), 6.847, 0.25],
             #[('Na','Cl'), 8.216, 0.5],
             #[('Ca','O'),  8.481, 0.5],
             #[('Mg','O'),  8.025, 0.5],
             ]

from AutoFlow.fccbatch import run_DFT

run_DFT('')