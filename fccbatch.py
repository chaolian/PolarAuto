#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  8 11:21:30 2021

@author: clian
"""
import os
from AutoFlow.pp import postprocess
from AutoFlow.file_heads import atomic_masses, atomic_numbers
from AutoFlow.file_heads import system_line, position_line, control_line
from AutoFlow.file_heads import kpoint_line, get_nscf_mesh
from AutoFlow.file_heads import setting, kpoint_band, ph_file, ph_g_file
from AutoFlow.file_heads import epw_wan_in, epw_dvscf_in, path_qpt, q2r_line
from AutoFlow.file_heads import dynmat_line, matdyn_line, GX_line
from AutoFlow.file_heads import epw_gtest_in, epw_wfc_in, lattice

def run(command, dryrun = True):
    if not dryrun: os.system(command)

def get_nbnd(compound, nbnd_unocc = 10):        
    # Get 
    nelectron = 0
    for element in compound:
        psedo  = element +'.upf'
        nElement = [float(line.split()[-1][:-1]) for line in 
                    open(os.environ["ESPRESSO_PSEUDO"] + '/' + psedo) 
                    if 'z_valence=' in line][-1]
        nelectron += nElement
    
    nbnd_occ = int(nelectron)//2
    print(nelectron, nbnd_occ)
    nbnd = nbnd_occ + nbnd_unocc #int(nelectron)
    
    return nbnd_occ, nbnd

def run_EPW(settings):
    for compound, alat, pos, nexv, nexc  in structSet: #['Ga','As'], ['Si', 'Si']: #['B','N'],
        root_dir = os.path.abspath('.')
        dirName = compound[0] if compound[0] == compound[1] else ''.join(compound)
        nbnd_occ, nbnd = get_nbnd(compound)
        print(nbnd_occ, nbnd, dirName)
        os.chdir(dirName)
        if not os.path.exists('epw'): os.mkdir('epw')
        os.chdir('epw')
        ###################################### EPW HOLE ################################################
        for itp, polaron_type in enumerate(['epolaron', 'hpolaron']):
            if not os.path.exists(polaron_type): os.mkdir(polaron_type)
            os.chdir(polaron_type)
            os.system('ln -s ../../ph/save . 2>/dev/null')
            os.system('ln -s ../../nscf/pwscf.save . 2>/dev/null')
            open('path.qpt', 'w').write(path_qpt)
            open('GX', 'w').write(GX_line)
            bands_skipped_line = "bands_skipped = 'exclude_bands = 1:%i, %i:%i'"
            if itp == 0:
                nbndsub = nexc
                addline = bands_skipped_line%(nbnd_occ, nbnd_occ + nbndsub + 1, nbnd)
            else:
                nbndsub = nexv
                if nbnd_occ > nbndsub:
                    addline = bands_skipped_line%(nbnd_occ - nbndsub, nbnd_occ+1, nbnd)
                else:
                    addline = "bands_skipped = 'exclude_bands = %i:%i'"%(nbnd_occ+1, nbnd)
                
            #'%s:sp3'%compound[0]
            proj = "auto_projections   = .true. \n  scdm_proj   = .true.  "
            #proj = "  proj(1)       = %s:sp3"%compound[0]
            wan_file = epw_wan_in%(nbndsub, addline, proj, *nkpoints, *nqpoints)
            
            open('epw.wan.in', 'w').write(wan_file)
            command = '$MPIRUN -np %i $EPW/bin/epw.x -nk %i -i epw.wan.in | tee epw.wan.out'%(np, nk)
            run(command, dryrun)
            
            wan_file = epw_dvscf_in%(nbndsub, addline, *nkpoints, *nqpoints)
            open('epw.dvscf.in', 'w').write(wan_file)
            command = '$MPIRUN -np %i $EPW/bin/epw.x -nk %i -i epw.dvscf.in | tee epw.dvscf.out'%(np, nk)
            run(command, dryrun)
            
            wan_file = epw_gtest_in%(nbndsub, *nkpoints, *nqpoints)
            open('epw.gtest.in', 'w').write(wan_file)
            command = '$MPIRUN -np %i $EPW/bin/epw.x -nk %i -i epw.gtest.in | tee epw.gtest.out'%(np, nk)
            run(command, dryrun)
            
            filelist = ['epwdata.fmt', 'selecq.fmt', 'path.qpt', 'pwscf.epmatwp',
                        'pwscf.kmap','crystal.fmt','vmedata.fmt','pwscf_*.cube',
                        'pwscf.kgmap','pwscf.ukk']
            for ik in range(1, nk_plrn):
                dirName = '%i'%ik
                if not os.path.exists(dirName): os.mkdir(dirName)
                os.chdir(dirName)
                #print(epw_wfc_in)
                wan_file = epw_wfc_in%(nbndsub, 1, *nkpoints, *nqpoints, *[ik]*6)
                for file in filelist:
                    os.system('ln -s ../%s . 2>/dev/null'%file)
                open('epw.wfc.in', 'w').write(wan_file)
                npk = min(np, ik*ik)
                command = '$MPIRUN -np %i $EPW/bin/epw.x -nk %i -i epw.wfc.in | tee epw.wfc.out'%(npk, npk)
                #print(command)
                run(command, dryrun)
                os.chdir('..')
            
            os.chdir('..')
        os.chdir(root_dir)
        
    

def run_DFT(settings):
    root_dir = os.path.abspath('.')
    for compound, alat, pos, nexv, nexc  in structSet: #['Ga','As'], ['Si', 'Si']: #['B','N'],
        if compound[0] == compound[1]:
            dirName = compound[0]
        else:
            dirName = ''.join(compound)
        
        ###################################### Build Input ###########################################
        print("Calculating the structure : ", dirName)
        if not os.path.exists(dirName):
            os.mkdir(dirName)
        os.chdir(dirName)
        
        elements = []
        for element in compound:
            index = atomic_numbers[element]
            elements.append([element, index, atomic_masses[index]])
        
        numElement = len(elements)
        
        species = 'ATOMIC_SPECIES \n'
        for i, element in enumerate(elements):
            psedo  = element[0] +'.upf'
            species += "%s \t %.4f \t %s\n" % (element[0], element[2], psedo)
            
        nbnd_occ, nbnd = get_nbnd(compound)
        ###################################### Optimization ###########################################
        if run_opti:
            if not os.path.exists('opti'): os.mkdir('opti')
            os.chdir('opti')
            
            system = system_line%(numElement, ecut, alat, '') 
            position = position_line%(compound[0], compound[1], pos, pos, pos)
            control = control_line%('vc-relax')
            kpoint= kpoint_line%(nkpoints)
            
            open('opti.in','w').write(control + system + setting + species + position + kpoint)
            command = '$MPIRUN -np %i $EPW/bin/pw.x -nk %i -i opti.in | tee opti.out'%(np, nk)
            
            run(command, dryrun)
            if dryrun:
                new_alat = alat
            else:
                new_alat = [float(line.split()[-1]) for line in open('opti.out').readlines() 
                           if 'celldm(1) =' in line[:]][-1]
            print("Lattice constant after optimization: ", new_alat)
            os.chdir('..')
        ########################################## SCF #################################################
        if run_scf:
            if not os.path.exists('scf'): os.mkdir('scf')
            os.chdir('scf')
            
            control = control_line%('scf')
            system = system_line%(numElement, ecut, new_alat, '')
            open('scf.in','w').write(control + system + setting + species + position + kpoint)
            command = '$MPIRUN -np %i $EPW/bin/pw.x -nk %i -i scf.in | tee scf.out'%(np, nk)
            #os.system('cp -r pwscf.save scf.save/')
            run(command, dryrun)
            os.chdir('..')
        ######################################### BAND ################################################
        if run_band:
            if not os.path.exists('bands'): os.mkdir('bands')
            os.system('cp -r scf/pwscf.save bands/')
            os.chdir('bands')
            
            control = control_line%('bands')
            system = system_line%(numElement, ecut, alat, 'nbnd            = %i'%nbnd)
            open('nscf_band.in','w').write(control + system + setting + species + position + kpoint_band)
            command = '$MPIRUN -np %i $EPW/bin/pw.x -nk %i -i nscf_band.in | tee nscf_band.out'%(np, nk)
            run(command, dryrun)
            
            open('bands.in','w').writelines('&bands/\n')
            command = '$MPIRUN -np 1 $EPW/bin/bands.x -i bands.in'
            run(command, dryrun)
            
            os.chdir('..')
        ######################################### nscf #################################################
        if run_nscf:
            if not os.path.exists('nscf'): os.mkdir('nscf')
            os.system('cp -r scf/pwscf.save nscf/')
            os.chdir('nscf')
            
            control = control_line%('bands')
            system = system_line%(numElement, ecut, alat, 'nbnd            = %i'%nbnd)
            kpoint_nscf = get_nscf_mesh(nkpoints)
            #print(kpoint_nscf)
            open('nscf.in','w').write(control + system + setting + species + position + kpoint_nscf)
            command = '$MPIRUN -np %i $EPW/bin/pw.x -nk %i -i nscf.in | tee nscf.out'%(np, nk)
            run(command, dryrun)
            os.chdir('..')
        ######################################## PHonon ################################################
        if run_ph:
            if not os.path.exists('ph'): os.mkdir('ph')
            os.chdir('ph')
            recover = '.true.' if os.path.exists('_ph0') else '.false.'
            os.system('ln -s ../scf/pwscf.save . 2>/dev/null')
            
            open('ph.in','w').write(ph_file%(recover, *nqpoints))
            command = '$MPIRUN -np %i $EPW/bin/ph.x -nk %i -i ph.in | tee ph.out'%(np, nk)
            
            run(command, dryrun)
            
            open('q2r.in', 'w').write(q2r_line)
            command = '$MPIRUN -np 1 $EPW/bin/q2r.x -i q2r.in | tee q2r.out'
            run(command, dryrun)
            
            open('matdyn.in', 'w').write(matdyn_line)
            command = '$MPIRUN -np 1 $EPW/bin/matdyn.x -i matdyn.in | tee matdyn.out'
            run(command, dryrun)
            
            open('dynmat.in', 'w').write(dynmat_line)
            command = '$MPIRUN -np 1 $EPW/bin/dynmat.x -i dynmat.in | tee dynmat.out'
            run(command, dryrun)
            
            if dryrun:
                if not os.path.exists('save'): os.mkdir('save')
            else:
                postprocess('pwscf')
            os.chdir('..')
        ###################################### G from PH ###############################################
        if run_g_ph:
            if not os.path.exists('dfpt-g'): os.mkdir('dfpt-g')
            os.chdir('dfpt-g')
            recover = '.true.' if os.path.exists('_ph0') else '.false.'
            control = control_line%('scf')
            system = system_line%(numElement, ecut, new_alat, 'nbnd            = %i'%nbnd)
            open('scf.in','w').write(control + system + setting + species + position + kpoint)
            command = '$MPIRUN -np %i $EPW/bin/pw.x -nk %i -i scf.in | tee scf.out'%(np, nk)
            #os.system('cp -r pwscf.save scf.save/')
            run(command, dryrun)
            
            open('ph_line.in','w').write(ph_g_file%(recover, nqline))
            command = '$MPIRUN -np %i $EPW/bin/ph.x -nk %i -i ph_line.in >> ph_line.out'%(np, nk)
            
            run(command, dryrun)
            
            os.chdir('..')
        os.chdir(root_dir)
        
        



if __name__ == '__main__':
    pass
    
